{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    pandoc
    texlive.combined.scheme-medium
  ];
}
